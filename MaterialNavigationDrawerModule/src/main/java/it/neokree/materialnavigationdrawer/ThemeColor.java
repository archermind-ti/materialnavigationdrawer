package it.neokree.materialnavigationdrawer;

import ohos.agp.utils.Color;

public class ThemeColor {
    public static final int COLOR_MODE_LIGHT_BG = Color.getIntColor("#FFFFFF");
    public static final int COLOR_MODE_DARK_BG = Color.getIntColor("#222222");
    public static final int COLOR_MODE_LIGHT_TEXT = Color.getIntColor("#222222");
    public static final int COLOR_MODE_DARK_TEXT = Color.getIntColor("#FFFFFF");
}
