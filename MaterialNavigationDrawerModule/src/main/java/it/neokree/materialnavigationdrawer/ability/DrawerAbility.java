package it.neokree.materialnavigationdrawer.ability;

import it.neokree.materialnavigationdrawer.slice.DrawerAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.global.resource.solidxml.Theme;

public class DrawerAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DrawerAbilitySlice.class.getName());
    }
}
