package it.neokree.materialnavigationdrawer.elements;

import ohos.global.resource.ResourceManager;
import ohos.media.image.PixelMap;

public class MaterialAccount {
    private String title = "";
    private String subTitle = "";
    private int accountNumber;
    private int photo;
    private int background;
    private PixelMap circularPhoto;

    public MaterialAccount(ResourceManager manager, String title, String subTitle, int photo, int background) {
        this.title = title;
        this.subTitle = subTitle;
        this.photo = photo;
        this.background = background;
    }

    public String getTitle() {
        return title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getPhoto() {
        return photo;
    }

    public int getBackground() {
        return background;
    }

    public PixelMap getCircularPhoto() {
        return circularPhoto;
    }
}