package it.neokree.materialnavigationdrawer.elements;

import it.neokree.materialnavigationdrawer.ResourceTable;
import it.neokree.materialnavigationdrawer.ThemeColor;
import it.neokree.materialnavigationdrawer.elements.listeners.MaterialSectionListener;
import it.neokree.materialnavigationdrawer.util.ColorUtil;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.render.BlendMode;
import ohos.agp.render.ColorMatrix;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class MaterialSection implements Component.ClickedListener {
    public static final int TARGET_FRAGMENT = 0;
    public static final int TARGET_ACTIVITY = 1;
    public static final int TARGET_LISTENER = 2;

    public static final int ICON_NO_ICON = 0;
    public static final int ICON_24DP = 1;
    public static final int ICON_40DP = 2;
    private Component component;
    private Text text;
    private Text notifications;
    private Image icon;
    private Element iconElement;

    private MaterialSectionListener listener;
    private Fraction targetFragment;
    private Intent targetIntent;
    private MaterialSectionListener targetListener;
    private String title;
    private boolean hasSectionColor;
    private boolean hasColorDark;
    private Element colorSelected = ColorUtil.getShapeElement(0x0A, 0, 0, 0);
    private Element colorUnSelected;
    private int sectionColor;
    private Color normalTextColor;
    private int colorDark;
    private int target;
    private boolean isSelected;
    private boolean realColor;
    private static final ColorMatrix sGrayFilter = new ColorMatrix(new float[]{
            0.130F, 0.130F, 0.130F, 0, 0,
            0.130F, 0.130F, 0.130F, 0, 0,
            0.130F, 0.130F, 0.130F, 0, 0,
            0, 0, 0, 1, 0
    });
    private static ColorMatrix selectedFilter;

    public MaterialSection(Context ctx, int iconType, boolean hasRippleSupport, int target) {
        switch (iconType) {
            case ICON_NO_ICON:
                component = LayoutScatter.getInstance(ctx).parse(ResourceTable.Layout_layout_material_section, null, false);
                text = (Text) component.findComponentById(ResourceTable.Id_section_text);
                notifications = (Text) component.findComponentById(ResourceTable.Id_section_notification);
                break;
            case ICON_24DP:
                component = LayoutScatter.getInstance(ctx).parse(ResourceTable.Layout_layout_material_section_icon, null, false);
                text = (Text) component.findComponentById(ResourceTable.Id_section_text);
                icon = (Image) component.findComponentById(ResourceTable.Id_section_icon);
                notifications = (Text) component.findComponentById(ResourceTable.Id_section_notification);
                break;
            case ICON_40DP:
                component = LayoutScatter.getInstance(ctx).parse(ResourceTable.Layout_layout_material_section_icon_large, null, false);
                text = (Text) component.findComponentById(ResourceTable.Id_section_text);
                icon = (Image) component.findComponentById(ResourceTable.Id_section_icon);
                notifications = (Text) component.findComponentById(ResourceTable.Id_section_notification);
                break;
        }
        component.setClickedListener(this);
        colorUnSelected = component.getBackgroundElement();
        normalTextColor = text.getTextColor();
        isSelected = false;
        realColor = false;
        this.target = target;
    }

    public void setThemeTextColor(boolean isLightMode) {
        if (!isLightMode) {
            component.setBackground(ColorUtil.getElement(ThemeColor.COLOR_MODE_DARK_BG));
            normalTextColor = ColorUtil.getColor(ThemeColor.COLOR_MODE_DARK_TEXT);
            text.setTextColor(normalTextColor);
        }
    }

    public void setOnClickListener(MaterialSectionListener listener) {
        this.listener = listener;
    }

    public void setTitle(String title) {
        this.title = title;
        this.text.setText(title);
    }

    public void setTarget(Fraction target) {
        this.targetFragment = target;
    }

    public void setTarget(Intent target) {
        this.targetIntent = target;
    }

    public void setTarget(MaterialSectionListener target) {
        this.targetListener = target;
    }


    public void setIcon(Element icon) {
        iconElement = icon;
        this.icon.setImageElement(icon);
        if (realColor) {
            icon.setStateColorMode(BlendMode.DST_IN);
            icon.setColorMatrix(sGrayFilter);
        }
    }

    public MaterialSection setSectionColor(int color) {
        hasSectionColor = true;
        sectionColor = color;

        return this;
    }

    public MaterialSection useRealColor() {
        realColor = true;
        return this;
    }

    public Image getIcon() {
        return icon;
    }

    public Text getText() {
        return text;
    }

    public Element getIconElement() {
        return iconElement;
    }

    public MaterialSectionListener getListener() {
        return listener;
    }

    public Fraction getTargetFragment() {
        return targetFragment;
    }

    public Intent getTargetIntent() {
        return targetIntent;
    }

    public MaterialSectionListener getTargetListener() {
        return targetListener;
    }

    public String getTitle() {
        return title;
    }

    public int getTarget() {
        return target;
    }

    public MaterialSection setSectionColor(int color, int colorDark) {
        setSectionColor(color);
        hasColorDark = true;
        this.colorDark = colorDark;

        return this;
    }

    public boolean isHasSectionColor() {
        return hasSectionColor;
    }

    public int getSectionColor() {
        return sectionColor;
    }

    public Component getComponent() {
        return component;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
        component.setBackground(this.isSelected ? colorSelected : colorUnSelected);
        text.setTextColor(hasSectionColor && this.isSelected ? ColorUtil.getColor(sectionColor) : normalTextColor);
    }

    @Override
    public void onClick(Component component) {
        afterClick();
    }


    private void afterClick() {
        setSelected(true);
        if (listener != null) {
            listener.onClick(this);
        }
    }
}
