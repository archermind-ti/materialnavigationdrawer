package it.neokree.materialnavigationdrawer.provider;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import it.neokree.materialnavigationdrawer.ResourceTable;
import it.neokree.materialnavigationdrawer.ThemeColor;
import it.neokree.materialnavigationdrawer.elements.MaterialAccount;
import it.neokree.materialnavigationdrawer.elements.MaterialSection;
import it.neokree.materialnavigationdrawer.util.ColorUtil;
import it.neokree.materialnavigationdrawer.widget.RoundImage;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;

public class AccountProvider extends BaseItemProvider {
    private List<Object> mData;
    private int selectPosition = -1;
    private boolean isLightTheme = true;

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public Object getItem(int i) {
        return mData != null && i >= 0 && i < mData.size() ? mData.get(i) : null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemComponentType(int position) {
        Object item = getItem(position);
        return item instanceof MaterialAccount
                ? 0
                : item instanceof MaterialSection
                ? 1
                : -1;
    }

    SectionHolder sectionHolder;

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Object item = getItem(i);
        int type = getItemComponentType(i);
        ViewHolder holder;

        Component root;
        switch (type) {
            case 0: {
                //账户
                if (component == null) {
                    holder = new ViewHolder();
                    component = LayoutScatter.getInstance(componentContainer.getContext())
                            .parse(ResourceTable.Layout_list_item_drawer_account, componentContainer, false);
                    holder.img = (RoundImage) component.findComponentById(ResourceTable.Id_section_icon);
                    holder.text = (Text) component.findComponentById(ResourceTable.Id_section_text);
                    if (!isLightTheme) {
                        holder.text.setTextColor(ColorUtil.getColor(ThemeColor.COLOR_MODE_DARK_TEXT));
                    }
                    component.setTag(holder);
                } else {
                    holder = (ViewHolder) component.getTag();
                }
                MaterialAccount account = (MaterialAccount) item;
                holder.text.setText(account.getTitle());
                holder.img.setPixelMapAndCircle(account.getPhoto());
                root = component;
            }
            break;
            case 1: {
                //自定义账户设置
                MaterialSection section = (MaterialSection) item;
                if (sectionHolder == null) {
                    sectionHolder = new SectionHolder();
                    sectionHolder.img = section.getIcon();
                    sectionHolder.text = section.getText();
                }

                sectionHolder.text.setText(section.getTitle());
                sectionHolder.img.setImageElement(section.getIconElement());
                root = section.getComponent();
            }
            break;
            default:
                root = null;
                break;
        }
        return selectPosition == i ? null : root;
    }

    static class SectionHolder {
        private Image img;
        private Text text;
    }

    static class ViewHolder {
        private RoundImage img;
        private Text text;
    }

    public List<Object> getData() {
        return mData;
    }

    public void setData(List<Object> mData) {
        this.mData = mData;
    }

    public void setLightTheme(boolean lightTheme) {
        isLightTheme = lightTheme;
    }

    public void setSelectPosition(int position) {
        selectPosition = position;
    }

    public void addData(Object obj) {
        addData(obj, getCount() - 1);
    }

    public void addData(Object obj, int position) {
        if (mData == null) {
            mData = new ArrayList<>();
        }
        if (position >= 0 && position < getCount()) {
            mData.add(position, obj);
        } else {
            mData.add(obj);
        }
    }

    public void removeData(Object obj) {
        if (!isEmpty()) {
            mData.remove(obj);
        }
    }

    public void removeData(int position) {
        if (!isEmpty() && position >= 0 && position < getCount()) {
            mData.remove(position);
        }
    }

    public void clearData() {
        if (!isEmpty()) {
            mData = null;
        }
    }

    public boolean isEmpty() {
        return mData == null || mData.isEmpty();
    }
}
