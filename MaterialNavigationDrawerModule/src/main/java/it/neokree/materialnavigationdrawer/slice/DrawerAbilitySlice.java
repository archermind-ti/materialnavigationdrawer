package it.neokree.materialnavigationdrawer.slice;

import it.neokree.materialnavigationdrawer.ResourceTable;
import it.neokree.materialnavigationdrawer.ThemeColor;
import it.neokree.materialnavigationdrawer.elements.MaterialAccount;
import it.neokree.materialnavigationdrawer.elements.MaterialSection;
import it.neokree.materialnavigationdrawer.elements.listeners.MaterialAccountListener;
import it.neokree.materialnavigationdrawer.elements.listeners.MaterialSectionListener;
import it.neokree.materialnavigationdrawer.provider.AccountProvider;
import it.neokree.materialnavigationdrawer.util.ApplicationUtil;
import it.neokree.materialnavigationdrawer.util.ColorUtil;
import it.neokree.materialnavigationdrawer.util.ResourceUtil;
import it.neokree.materialnavigationdrawer.widget.RoundImage;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;

import java.util.ArrayList;
import java.util.List;


public abstract class DrawerAbilitySlice extends AbilitySlice implements MaterialSectionListener {
    private ComponentContainer drawer;
    private ComponentContainer drawerHeader;
    private ComponentContainer drawerCenter;
    private ComponentContainer drawerBottom;
    private boolean running = false;
    private float screenWidth;
    protected boolean drawerOpen;
    private Component drawerBackground;
    //抽屉背景
    private Element backgroundElement;
    //抽屉背景透明度
    private final int background_alpha = 0x88;
    private RoundImage imgHeader1;
    private RoundImage imgHeader2;
    private RoundImage imgHeader3;
    private Image imgArrow;
    private Image imgHeaderBackground;
    private Text textName;
    private Text textMail;
    private boolean animRunning;
    private List<MaterialAccount> accountManager;
    private List<MaterialSection> accountSectionList;
    private List<MaterialSection> sectionList;
    private List<MaterialSection> bottomSectionList;
    private MaterialAccountListener accountListener;
    protected MaterialSection currentSection;
    private Component titleBar;
    private Element titleBarElement;
    private Text textTitle;
    private boolean accountsShow = false;
    private ComponentContainer drawerScrollView;
    private ListContainer drawerListView;
    private AccountProvider provider;
    private boolean isLightMode = true;
    private Component customDrawerHeaderView;
    private boolean canChangeTitleBarBg = true;
    protected boolean backToFirst = false;

    public abstract void init(Intent intent);

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_drawer);
        screenWidth = ApplicationUtil.getScreenWidth(this);
        drawerOpen = true;
        initView();
        initDrawerView();
        init(intent);
        initData();

        if (!drawerOpen) {
            backgroundElement.setAlpha(0);
            drawer.setTranslationX(-screenWidth);
        }
    }

    private void initView() {
        textTitle = (Text) findComponentById(ResourceTable.Id_textTitle);
        titleBar = findComponentById(ResourceTable.Id_titleBar);
        titleBarElement = titleBar.getBackgroundElement();
//        ComponentContainer fraction = (ComponentContainer) findComponentById(ResourceTable.Id_fraction);
        findComponentById(ResourceTable.Id_layoutMenu).setClickedListener(clickedListener);
    }

    private void initDrawerView() {
        //initDrawer
        drawer = (ComponentContainer) findComponentById(ResourceTable.Id_drawerLayout);
//        drawer.setBackground(ColorUtil.getElement(isLightMode ? COLOR_MODE_LIGHT_BG : COLOR_MODE_DARK_BG));
        drawerHeader = (ComponentContainer) findComponentById(ResourceTable.Id_drawerHeader);
        drawerScrollView = (ComponentContainer) findComponentById(ResourceTable.Id_drawerScrollView);
        drawerListView = (ListContainer) findComponentById(ResourceTable.Id_drawerListView);
        drawerCenter = (ComponentContainer) findComponentById(ResourceTable.Id_drawerCenter);
        drawerBottom = (ComponentContainer) findComponentById(ResourceTable.Id_drawerBottom);
        drawerBackground = findComponentById(ResourceTable.Id_drawerBackground);
        backgroundElement = ColorUtil.getShapeElement(background_alpha, 56, 78, 78);
        drawerBackground.setBackground(backgroundElement);
        drawer.setClickedListener(clickedListener);
        drawerBackground.setClickedListener(clickedListener);

        //init Account
        textName = (Text) findComponentById(ResourceTable.Id_textName);
        textMail = (Text) findComponentById(ResourceTable.Id_textMail);
        imgArrow = (Image) findComponentById(ResourceTable.Id_imgArrow);
        imgHeaderBackground = (Image) findComponentById(ResourceTable.Id_imgHeaderBackground);
        imgHeader1 = (RoundImage) findComponentById(ResourceTable.Id_imgHeader);
        imgHeader2 = (RoundImage) findComponentById(ResourceTable.Id_imgHeader1);
        imgHeader3 = (RoundImage) findComponentById(ResourceTable.Id_imgHeader2);

        imgHeader1.setClickedListener(clickedListener);
        imgHeader2.setClickedListener(clickedListener);
        imgHeader3.setClickedListener(clickedListener);
        imgArrow.setClickedListener(clickedListener);
        imgHeaderBackground.setClickedListener(clickedListener);
    }

    private void initData() {
        setDrawerHeader();
        setAccountList();
        setDrawerCenter();
        setDrawerBottom();
    }

    private void setDrawerHeader() {
        if (customDrawerHeaderView != null) {
            drawerHeader.removeAllComponents();
            drawerHeader.addComponent(customDrawerHeaderView);
            return;
        }
        imgArrow.setVisibility(Component.HIDE);
        imgHeader1.setVisibility(Component.HIDE);
        imgHeader2.setVisibility(Component.HIDE);
        imgHeader3.setVisibility(Component.HIDE);
        if (accountManager == null) {
            return;
        }

        for (int i = 0; i < accountManager.size(); i++) {
            drawerHeader.setVisibility(Component.VISIBLE);
            MaterialAccount account = accountManager.get(i);
            switch (i) {
                case 0: {
                    imgHeader1.setPixelMapAndCircle(account.getPhoto());
                    imgHeader1.setVisibility(Component.VISIBLE);
                    imgHeaderBackground.setImageElement(ResourceUtil.getPixelMapElement(this, account.getBackground()));
                    textName.setText(account.getTitle());
                    textMail.setText(account.getSubTitle());
                    imgHeader1.setTag(account);
                }
                break;
                case 1: {
                    imgHeader2.setPixelMapAndCircle(account.getPhoto());
                    imgHeader2.setVisibility(Component.VISIBLE);
                    imgArrow.setVisibility(Component.VISIBLE);
                    imgHeader2.setTag(account);
                }
                break;
                case 2: {
                    imgHeader3.setPixelMapAndCircle(account.getPhoto());
                    imgHeader3.setVisibility(Component.VISIBLE);
                    imgHeader3.setTag(account);
                }
                break;
                default:
                    break;
            }
        }
    }

    private void setAccountList() {
        if (accountManager == null && accountSectionList == null) {
            return;
        }
        drawerHeader.setVisibility(Component.VISIBLE);
        if (provider == null) {
            List<Object> list = new ArrayList<>();
            if (accountManager != null) {
                list.addAll(accountManager);
            }
            if (accountSectionList != null) {
                list.addAll(accountSectionList);
            }
            provider = new AccountProvider();
            if (!isLightMode) {
                provider.setLightTheme(false);
            }
            drawerListView.setItemProvider(provider);
            provider.setData(list);
            drawerListView.setItemClickedListener((listContainer, component, i, l) -> {
                //列表点击
                Object item = provider.getItem(i);
                if (item instanceof MaterialAccount) {
                    syncAccountHeader(i);
                    doArrowImgClick();
                } else if (item instanceof MaterialSection) {
                    doCustomAccountClick(i);
                }
                startAnimation();
            });
        }
        provider.setSelectPosition(0);
        provider.notifyDataChanged();
    }

    private void setDrawerCenter() {
        if (sectionList == null || sectionList.isEmpty()) {
            return;
        }
        for (MaterialSection section : sectionList) {
            if (!isLightMode) {
                section.setThemeTextColor(false);
            }
            drawerCenter.addComponent(section.getComponent());
        }

        if (currentSection != null) {
            setSelectedContent(currentSection);
            setFraction(currentSection);
        }
    }

    private void setDrawerBottom() {
        if (bottomSectionList == null || bottomSectionList.isEmpty()) {
            return;
        }
        for (MaterialSection section : bottomSectionList) {
            if (!isLightMode) {
                section.setThemeTextColor(false);
            }
            drawerBottom.addComponent(section.getComponent());
        }
    }

    private final Component.ClickedListener clickedListener = component -> {
        if (running || animRunning) {
            return;
        }
        int id = component.getId();
        if (/*id == ResourceTable.Id_drawerLayout ||*/ id == ResourceTable.Id_drawerBackground) {
            //点击抽屉背景，收起抽屉
            startAnimation();
        } else if (id == ResourceTable.Id_layoutMenu) {
            //打开抽屉
            startAnimation();
        } else if (id == ResourceTable.Id_imgHeader) {
            //doClick(imgHeader1);
        } else if (id == ResourceTable.Id_imgHeader1) {
            doHeaderClick(imgHeader2);
        } else if (id == ResourceTable.Id_imgHeader2) {
            doHeaderClick(imgHeader3);
        } else if (id == ResourceTable.Id_imgArrow) {
            //展示/隐藏 账户信息列表
            doArrowImgClick();
        } else if (id == ResourceTable.Id_imgHeaderBackground) {
            //等同于点击 ResourceTable.Id_imgArrow
            if (imgArrow.getVisibility() == Component.VISIBLE) {
                doArrowImgClick();
            }
        }
    };

    /**
     * 展示/隐藏 账户信息列表
     */
    private void doArrowImgClick() {
        accountsShow = !accountsShow;
        if (accountsShow && accountListener != null) {
            accountListener.onAccountOpening((MaterialAccount) imgHeader1.getTag());
        }
        drawerScrollView.setVisibility(accountsShow ? Component.HIDE : Component.VISIBLE);
        drawerListView.setVisibility(accountsShow ? Component.VISIBLE : Component.HIDE);
        imgArrow.setImageElement(ResourceUtil.getPixelMapElement(this,
                accountsShow ?
                        ResourceTable.Media_ic_arrow_drop_up_white_24dp :
                        ResourceTable.Media_ic_arrow_drop_down_white_24dp
        ));
        if (provider != null) {
            provider.notifyDataChanged();
        }
    }

    private void doCustomAccountClick(int position) {
        MaterialSection section = (MaterialSection) provider.getItem(position);
        MaterialSectionListener listener = section.getTargetListener();
        if (listener != null) {
            listener.onClick(section);
        }
    }

    /**
     * 账户列表点击时同步头像
     */
    private void syncAccountHeader(int position) {
        //刷新账户列表
        MaterialAccount account = (MaterialAccount) provider.getItem(position);
        imgHeader1.setTag(account);
        textName.setText(account.getTitle());
        textMail.setText(account.getSubTitle());
        imgHeader1.setPixelMapAndCircle(account.getPhoto());
        imgHeaderBackground.setImageElement(ResourceUtil.getPixelMapElement(this, account.getBackground()));
        provider.setSelectPosition(position);
        provider.notifyDataChanged();

        //刷新头部头像
        int tem = 0;
        for (int i = 0; i < accountManager.size(); i++) {
            if (position == i) {
                continue;
            }
            if (tem > 1) {
                break;
            }
            MaterialAccount acc = accountManager.get(i);
            if (tem == 0) {
                imgHeader2.setTag(acc);
                imgHeader2.setPixelMapAndCircle(acc.getPhoto());
            } else if (tem == 1) {
                imgHeader3.setTag(account);
                imgHeader3.setPixelMapAndCircle(acc.getPhoto());
            }
            tem++;
        }
    }

    /**
     * 账户头像点击
     */
    private void doHeaderClick(RoundImage clickImg) {
        MaterialAccount currentAccount = (MaterialAccount) imgHeader1.getTag();
        MaterialAccount clickAccount = (MaterialAccount) clickImg.getTag();
        Rect r1 = imgHeader1.getComponentPosition();
        Rect rClick = clickImg.getComponentPosition();
        float w1 = r1.getWidth();
        float h1 = r1.getHeight();
        float w2 = rClick.getWidth();
        float h2 = rClick.getHeight();

        imgHeader1.setPixelMapAndCircle(clickAccount.getPhoto());
        imgHeader1.setTranslation(rClick.getCenterX() - r1.getCenterX(), rClick.getCenterY() - r1.getCenterY());
        imgHeader1.setScale(w2 / w1, h2 / h1);
        clickImg.setPixelMapAndCircle(currentAccount.getPhoto());
        clickImg.setScale(0.1f, 0.1f);

        textName.setText(clickAccount.getTitle());
        textMail.setText(clickAccount.getSubTitle());
        imgHeaderBackground.setImageElement(ResourceUtil.getPixelMapElement(this, clickAccount.getBackground()));
        imgHeader1.setTag(clickAccount);
        clickImg.setTag(currentAccount);

        if (provider != null) {
            //刷新账户列表
            for (int i = 0; i < accountManager.size(); i++) {
                if (accountManager.get(i) == clickAccount) {
                    provider.setSelectPosition(i);
                    provider.notifyDataChanged();
                }
            }
        }

        AnimatorValue anim = new AnimatorValue();
        anim.setDuration(300);
        anim.setLoopedCount(0);
        anim.setValueUpdateListener((animatorValue, v) -> {
            clickImg.setScale(0.1f + v * (1.0f - 0.1f), 0.1f + v * (1.0f - 0.1f));

            imgHeader1.setTranslation((1.0f - v) * (rClick.getCenterX() - r1.getCenterX()),
                    (1.0f - v) * (rClick.getCenterY() - r1.getCenterY()));
            imgHeader1.setScale(w2 / w1 + v * (1.0f - w2 / w1),
                    h2 / h1 + v * (1.0f - h2 / h1));
        });
        anim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                animRunning = true;
            }

            @Override
            public void onStop(Animator animator) {
                animRunning = false;
                startAnimation();
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        anim.start();
        if (accountListener != null) {
            accountListener.onChangeAccount(clickAccount);
        }
    }

    /**
     * 抽屉开关动画
     */
    protected void startAnimation() {
        AnimatorValue anim = new AnimatorValue();
        anim.setDuration(200);
        anim.setCurveType(Animator.CurveType.LINEAR);
        anim.setValueUpdateListener((animatorValue, v) -> {
            float tranX = drawerOpen ? (-screenWidth * v) : (screenWidth * v - screenWidth);
            backgroundElement.setAlpha((int) (drawerOpen ? (background_alpha - background_alpha * v) : (background_alpha * v)));
            drawer.setTranslationX(tranX);
            drawerBackground.setBackground(backgroundElement);
        });
        anim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                running = true;
                if (!drawerOpen) {
                    drawer.setVisibility(Component.VISIBLE);
                    drawerBackground.setVisibility(Component.VISIBLE);
                }
            }

            @Override
            public void onStop(Animator animator) {
                running = false;
                drawerOpen = !drawerOpen;
                if (!drawerOpen) {
                    drawer.setVisibility(Component.HIDE);
                    drawerBackground.setVisibility(Component.HIDE);
                }
            }

            @Override
            public void onCancel(Animator animator) {

            }

            @Override
            public void onEnd(Animator animator) {
            }

            @Override
            public void onPause(Animator animator) {

            }

            @Override
            public void onResume(Animator animator) {

            }
        });
        anim.start();
    }

    @Override
    public void onActive() {
        super.onActive();
        if (provider != null) {
            provider.notifyDataChanged();
        }
        Object tag1 = imgHeader1.getTag();
        Object tag2 = imgHeader2.getTag();
        Object tag3 = imgHeader3.getTag();
        if (tag1 instanceof MaterialAccount) {
            imgHeader1.setPixelMapAndCircle(((MaterialAccount) tag1).getPhoto());
        }
        if (tag1 instanceof MaterialAccount) {
            imgHeader2.setPixelMapAndCircle(((MaterialAccount) tag2).getPhoto());
        }
        if (tag1 instanceof MaterialAccount) {
            imgHeader3.setPixelMapAndCircle(((MaterialAccount) tag3).getPhoto());
        }
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    public void addAccount(MaterialAccount account) {
        if (accountManager == null) {
            accountManager = new ArrayList<>();
        }
        accountManager.add(account);
    }

    public void addSection(MaterialSection section) {
        if (sectionList == null) {
            sectionList = new ArrayList<>();
        }
        sectionList.add(section);
    }

    public void addBottomSection(MaterialSection section) {
        if (bottomSectionList == null) {
            bottomSectionList = new ArrayList<>();
        }
        bottomSectionList.add(section);
    }

    public void addAccountSection(MaterialSection section) {
        if (accountSectionList == null) {
            accountSectionList = new ArrayList<>();
        }
        accountSectionList.add(section);
    }

    public void setAccountListener(MaterialAccountListener listener) {
        this.accountListener = listener;
    }

    public void setDrawerHeaderImage(Element element) {
        drawerHeader.setVisibility(Component.VISIBLE);
        imgHeaderBackground.setImageElement(element);
    }

    public void setDrawerHeaderImgClick(Component.ClickedListener click) {
        imgHeaderBackground.setClickedListener(click);
    }

    public void setUserEmail(String email) {
        textMail.setText(email);
    }

    public void setUserEmailTextColor(Color color) {
        textMail.setTextColor(color);
    }

    public void setUsername(String username) {
        this.textName.setText(username);
    }

    public void setUsernameTextColor(Color color) {
        this.textName.setTextColor(color);
    }

    public void setFirstAccountPhoto(Element photo) {
        imgHeader1.setImageElement(photo);
    }

    public void setSecondAccountPhoto(Element photo) {
        imgHeader2.setImageElement(photo);
    }

    public void setThirdAccountPhoto(Element photo) {
        imgHeader3.setImageElement(photo);
    }

    public void setDrawerBackgroundColor(Element element) {
        drawer.setBackground(element);
    }

    public void setBackPattern(int backPattern) {
    }

    public void setCanChangeTitleBarBg(boolean canChangeTitleBarBg) {
        this.canChangeTitleBarBg = canChangeTitleBarBg;
    }

    public void setDrawerHeaderCustom(Component view) {
        drawerHeader.setVisibility(Component.VISIBLE);
        customDrawerHeaderView = view;
    }

    // create sections

    public MaterialSection newSection(String title, Element icon, Fraction target) {
        MaterialSection section = new MaterialSection(this, MaterialSection.ICON_24DP, false, MaterialSection.TARGET_FRAGMENT);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSectionWithRealColor(String title, Element icon, Fraction target) {
        MaterialSection section = new MaterialSection(this, MaterialSection.ICON_24DP, false, MaterialSection.TARGET_FRAGMENT);
        section.setOnClickListener(this);
        section.useRealColor();
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Element icon, Intent target) {
        MaterialSection section = new MaterialSection(this, MaterialSection.ICON_24DP, false, MaterialSection.TARGET_ACTIVITY);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSectionWithRealColor(String title, Element icon, Intent target) {
        MaterialSection section = new MaterialSection(this, MaterialSection.ICON_24DP, false, MaterialSection.TARGET_ACTIVITY);
        section.setOnClickListener(this);
        section.useRealColor();
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Element icon, MaterialSectionListener target) {
        MaterialSection section = new MaterialSection(this, MaterialSection.ICON_24DP, false, MaterialSection.TARGET_LISTENER);
        section.setOnClickListener(this);
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSectionWithRealColor(String title, Element icon, MaterialSectionListener target) {
        MaterialSection section = new MaterialSection(this, MaterialSection.ICON_24DP, false, MaterialSection.TARGET_LISTENER);
        section.setOnClickListener(this);
        section.useRealColor();
        section.setIcon(icon);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Fraction target) {
        MaterialSection section = new MaterialSection(this, MaterialSection.ICON_NO_ICON, false, MaterialSection.TARGET_FRAGMENT);
        section.setOnClickListener(this);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, Intent target) {
        MaterialSection section = new MaterialSection(this, MaterialSection.ICON_NO_ICON, false, MaterialSection.TARGET_ACTIVITY);
        section.setOnClickListener(this);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    public MaterialSection newSection(String title, MaterialSectionListener target) {
        MaterialSection section = new MaterialSection(this, MaterialSection.ICON_NO_ICON, false, MaterialSection.TARGET_LISTENER);
        section.setOnClickListener(this);
        section.setTitle(title);
        section.setTarget(target);

        return section;
    }

    @Override
    public void onClick(MaterialSection section) {
        int target = section.getTarget();
        if (target == MaterialSection.TARGET_ACTIVITY) {
            startAbility(section.getTargetIntent());
            return;
        }
        if (section == currentSection) {
            startAnimation();
            return;
        }

        setSelectedContent(section);
        setFraction(section);
        if (currentSection != null) {
            currentSection.setSelected(false);
        }
        currentSection = section;

        startAnimation();
    }

    private void setSelectedContent(MaterialSection section) {
        textTitle.setText(section.getTitle());
        if (canChangeTitleBarBg) {
            titleBar.setBackground(section.isHasSectionColor() ?
                    ColorUtil.getElement(section.getSectionColor()) :
                    titleBarElement);
        }
    }

    public void setFraction(MaterialSection section) {
        Fraction fraction = section.getTargetFragment();
        if (fraction == null) {
            return;
        }
        FractionAbility ability = (FractionAbility) getAbility();
        FractionScheduler scheduler = ability.getFractionManager().startFractionScheduler();
        if (currentSection != null) {
            scheduler.remove(currentSection.getTargetFragment());
        }
        scheduler.add(ResourceTable.Id_fraction, section.getTargetFragment());
        scheduler.submit();
    }

    public Intent createIntent(String abilityName) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(abilityName)
                .build();
        intent.setOperation(operation);
        return intent;
    }

    public void setLightMode(boolean lightMode) {
        isLightMode = lightMode;

        findComponentById(ResourceTable.Id_root).setBackground(ColorUtil.getElement(isLightMode ? ThemeColor.COLOR_MODE_LIGHT_BG : ThemeColor.COLOR_MODE_DARK_BG));
        drawerScrollView.setBackground(ColorUtil.getElement(isLightMode ? ThemeColor.COLOR_MODE_LIGHT_BG : ThemeColor.COLOR_MODE_DARK_BG));
        drawerListView.setBackground(ColorUtil.getElement(isLightMode ? ThemeColor.COLOR_MODE_LIGHT_BG : ThemeColor.COLOR_MODE_DARK_BG));
        drawerBottom.setBackground(ColorUtil.getElement(isLightMode ? ThemeColor.COLOR_MODE_LIGHT_BG : ThemeColor.COLOR_MODE_DARK_BG));
        Text textTitle = (Text) findComponentById(ResourceTable.Id_textTitle);
        textTitle.setTextColor(ColorUtil.getColor(/*isLightMode ? ThemeColor.COLOR_MODE_LIGHT_TEXT : */ThemeColor.COLOR_MODE_DARK_TEXT));
    }

    @Override
    protected void onBackPressed() {
        if (backToFirst && sectionList != null && !sectionList.isEmpty() && currentSection != sectionList.get(0)) {
            MaterialSection section = sectionList.get(0);
            section.setSelected(true);
            setSelectedContent(section);
            setFraction(section);
            if (currentSection != null) {
                currentSection.setSelected(false);
            }
            currentSection = section;
        } else {
            super.onBackPressed();
        }
    }
}
