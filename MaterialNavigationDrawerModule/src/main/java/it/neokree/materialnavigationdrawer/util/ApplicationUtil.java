package it.neokree.materialnavigationdrawer.util;

import ohos.aafwk.ability.Ability;
import ohos.agp.utils.Point;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.NoSuchElementException;

public class ApplicationUtil {
    /**
     * 获取App的Context
     *
     * @param ability Ability
     * @return null or ApplicationContext
     */
    public static Context getApplication(Ability ability) {
        return ability == null ? null : ability.getApplicationContext();
    }

    /**
     * @param ability Ability
     * @return ”“ or App的包名
     */
    public static String getAppBundleName(Ability ability) {
        return ability == null ? "" : ability.getApplicationContext().getBundleName();
    }

    /**
     * @param context Context
     * @return 0 or screenWidth in pixel
     */
    public static float getScreenWidth(Context context) {
        float[] screenSize = getScreenSize(context);
        return screenSize == null ? 0F : screenSize[0];
    }

    /**
     * @param context Context
     * @return 0 or screenHeight in pixel
     */
    public static float getScreenHeight(Context context) {
        float[] screenSize = getScreenSize(context);
        return screenSize == null ? 0F : screenSize[1];
    }

    /**
     * @param context AbilityPackage
     * @return null or float[]{width , height},unit in pixel
     */
    public static float[] getScreenSize(Context context) {
        if (context == null) {
            return null;
        }
        //方法1
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getSize(point);
        float[] size = new float[2];
        size[0] = point.getPointX();
        size[1] = point.getPointY();

        return size;
    }

    /**
     * vp转像素
     *
     * @param context 上下文
     * @param vp      vp值
     * @return 0 or value from switch
     */
    public static int vp2px(Context context, float vp) {
        if (context == null) {
            return 0;
        }
        int result = 0;
        try {
            DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
            result = (int) (attributes.densityPixels * vp);
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
        return result;
    }

}
