package it.neokree.materialnavigationdrawer.util;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

/**
 * create by DaiL on 2021/4/27
 */
public class ColorUtil {
    public static ShapeElement getShapeElement(int red, int green, int blue) {
        return getShapeElement(255, red, green, blue);
    }

    public static ShapeElement getShapeElement(int alpha, int red, int green, int blue) {
        ShapeElement element = new ShapeElement();
        element.setRgbColor(getRgbColor(red, green, blue));
        element.setAlpha(alpha);
        return element;
    }

    public static Element getElement(int color) {
        RgbColor rgbColor = RgbColor.fromArgbInt(color);
        return getShapeElement(rgbColor.getAlpha(), rgbColor.getRed(), rgbColor.getGreen(), rgbColor.getBlue());
    }

    public static RgbColor getRgbColor(int red, int green, int blue) {
        return new RgbColor(red, green, blue);
    }

    public static Color getColor(int red, int green, int blue) {
        return getColor(255, red, green, blue);
    }

    public static Color getColor(int alpha, int red, int green, int blue) {
        return new Color(Color.argb(alpha, red, green, blue));
    }

    public static int getIntColor(int red, int green, int blue) {
        return getIntColor(255, red, green, blue);
    }

    public static int getIntColor(int alpha, int red, int green, int blue) {
        return Color.argb(alpha, red, green, blue);
    }

    public static Color getColor(int color) {
        RgbColor rgbColor = RgbColor.fromArgbInt(color);
        return getColor(rgbColor.getAlpha(), rgbColor.getRed(), rgbColor.getGreen(), rgbColor.getBlue());
    }
}
