package it.neokree.materialnavigationdrawer.util;
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.global.resource.Element;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.PixelMap;

import java.io.IOException;

public class ResourceUtil {
    /**
     * @param context Context
     * @param resId   资源id
     * @return Element or null
     */
    public static Element getElement(Context context, int resId) {
        Element element = null;
        try {
            element = context.getResourceManager().getElement(resId);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return element;
    }

    /**
     * @param context Context
     * @param resId   资源id
     * @return Element or null
     */
    public static PixelMap getPixelMap(Context context, int resId) {
        PixelMap pixelMap = null;
        try {
            PixelMapElement pixelMapElement = new PixelMapElement(context.getResourceManager().getResource(resId));
            pixelMap = pixelMapElement.getPixelMap();
        } catch (IOException | NotExistException e) {
            e.printStackTrace();
        }
        return pixelMap;
    }

    public static PixelMapElement getPixelMapElement(Context context, int resId) {
        PixelMapElement pixelMapElement = null;
        try {
            pixelMapElement = new PixelMapElement(context.getResourceManager().getResource(resId));
        } catch (IOException | NotExistException e) {
            e.printStackTrace();
        }
        return pixelMapElement;
    }
}
