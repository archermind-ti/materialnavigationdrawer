### MaterialNavigationDrawer

### 项目介绍

具有材料设计风格和简化方法的导航抽屉活动

### 效果图

<img src="/material.gif" style="zoom:33%;" />

### 安装教程

#### 方式一

1. 下载模块代码添加到自己的工程。
2. 关联使用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation project(':MaterialNavigationDrawerModule')
	……
}
```

#### 方式二

1. 在module的build.gradle中添加依赖

   ```groovy
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
       implementation 'com.gitee.archermind-ti:MaterialNavigationDrawer:1.0.0-beta2'
       ……
   }
   ```
2. 在project的build.gradle中添加`mavenCentral()`的引用

   ``` groovy
   allprojects {
       repositories {
           ……
           mavenCentral()
       }
   }
   ```

   
### 使用说明
#### 配置view 

布局xml里：
``` java
<com.balysv.materialripple.MaterialRippleLayout
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:margin="5vp">

        <Text
            ohos:id="$+id:text"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:layout_alignment="center"
            ohos:text="CLICK ME"
            ohos:text_size="16fp"
            ohos:text_weight="800"
            />

    </com.balysv.materialripple.MaterialRippleLayout>
```
### 版本迭代

* v1.0.0

### License

```

Apache License
                           Version 2.0, January 2004
                        http://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

	...
	...
	...

   Copyright {yyyy} {name of copyright owner}

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

```