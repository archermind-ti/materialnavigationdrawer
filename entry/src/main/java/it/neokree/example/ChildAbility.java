package it.neokree.example;

import it.neokree.example.slice.ChildAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class ChildAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ChildAbilitySlice.class.getName());
    }
}
