package it.neokree.example;

import it.neokree.example.slice.SettingsSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class Settings extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SettingsSlice.class.getName());
    }
}
