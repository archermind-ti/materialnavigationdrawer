package it.neokree.example.backpattern;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

public class BackAnywhere extends FractionAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(BackAnywhereSlice.class.getName());
    }
}
