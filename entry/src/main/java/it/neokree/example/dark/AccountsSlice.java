package it.neokree.example.dark;

import it.neokree.example.mockedfraction.FractionButton;
import it.neokree.example.mockedfraction.FractionIndex;
import it.neokree.materialnavigationdrawer.ResourceTable;
import it.neokree.materialnavigationdrawer.elements.MaterialAccount;
import it.neokree.materialnavigationdrawer.elements.MaterialSection;
import it.neokree.materialnavigationdrawer.elements.listeners.MaterialAccountListener;
import it.neokree.materialnavigationdrawer.slice.DrawerAbilitySlice;
import it.neokree.materialnavigationdrawer.util.ResourceUtil;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

public class AccountsSlice extends DrawerAbilitySlice implements MaterialAccountListener {

    @Override
    public void init(Intent intent) {
        // add accounts
        MaterialAccount account = new MaterialAccount(this.getResourceManager(),
                "NeoKree", "neokree@gmail.com",
                ResourceTable.Media_photo,
                ResourceTable.Media_bamboo);
        addAccount(account);

        MaterialAccount account2 = new MaterialAccount(this.getResourceManager(),
                "Hatsune Miky", "hatsune.miku@example.com",
                ResourceTable.Media_photo2,
                ResourceTable.Media_mat2);
        addAccount(account2);

        MaterialAccount account3 = new MaterialAccount(this.getResourceManager(),
                "Example", "example@example.com",
                ResourceTable.Media_photo,
                ResourceTable.Media_mat3);
        addAccount(account3);

        // set listener
        this.setAccountListener(this);

        // create sections
        MaterialSection section = newSection("Section 1", new FractionIndex(false));
        this.addSection(section);
        section.setSelected(true);
        currentSection = section;

        this.addSection(newSection("Section 2", new FractionIndex(false)));
        this.addSection(newSectionWithRealColor("Section 3",
                ResourceUtil.getPixelMapElement(this, ResourceTable.Media_ic_mic_white_24dp),
                new FractionButton(false)).setSectionColor(Color.getIntColor("#9c27b0")));
        this.addSection(newSection("Section",
                ResourceUtil.getPixelMapElement(this, ResourceTable.Media_ic_hotel_grey600_24dp),
                new FractionButton(false)).setSectionColor(Color.getIntColor("#03a9f4")));

        // create bottom section
        this.addBottomSection(newSection("Bottom Section",
                ResourceUtil.getPixelMapElement(this, ResourceTable.Media_ic_settings_black_24dp),
                createIntent("it.neokree.example.Settings")));

        setLightMode(false);
    }

    @Override
    public void onAccountOpening(MaterialAccount account) {

    }

    @Override
    public void onChangeAccount(MaterialAccount newAccount) {

    }
}
