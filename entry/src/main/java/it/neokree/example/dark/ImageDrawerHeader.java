package it.neokree.example.dark;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

public class ImageDrawerHeader extends FractionAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ImageDrawerHeaderSlice.class.getName());
    }
}
