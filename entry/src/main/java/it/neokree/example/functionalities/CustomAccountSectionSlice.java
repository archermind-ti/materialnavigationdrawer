package it.neokree.example.functionalities;

import it.neokree.example.mockedfraction.FractionButton;
import it.neokree.example.mockedfraction.FractionIndex;
import it.neokree.materialnavigationdrawer.ResourceTable;
import it.neokree.materialnavigationdrawer.elements.MaterialAccount;
import it.neokree.materialnavigationdrawer.elements.MaterialSection;
import it.neokree.materialnavigationdrawer.elements.listeners.MaterialSectionListener;
import it.neokree.materialnavigationdrawer.slice.DrawerAbilitySlice;
import it.neokree.materialnavigationdrawer.util.ResourceUtil;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;

public class CustomAccountSectionSlice extends DrawerAbilitySlice {
    @Override
    public void init(Intent intent) {
        // add accounts
        MaterialAccount account = new MaterialAccount(this.getResourceManager(),
                "NeoKree", "neokree@gmail.com",
                ResourceTable.Media_photo,
                ResourceTable.Media_bamboo);
        addAccount(account);

        MaterialAccount account2 = new MaterialAccount(this.getResourceManager(),
                "Hatsune Miky", "hatsune.miku@example.com",
                ResourceTable.Media_photo2,
                ResourceTable.Media_mat2);
        addAccount(account2);

        MaterialAccount account3 = new MaterialAccount(this.getResourceManager(),
                "Example", "example@example.com",
                ResourceTable.Media_photo,
                ResourceTable.Media_mat3);
        addAccount(account3);

        // add account sections
        addAccountSection(newSection("Account settings",
                ResourceUtil.getPixelMapElement(this, ResourceTable.Media_ic_settings_black_24dp)
                , new MaterialSectionListener() {
                    @Override
                    public void onClick(MaterialSection section) {
                        new ToastDialog(getContext()).setText("Account settings clicked").show();
                        // for default section is selected when you click on it
//                        section.unSelect(); // so deselect the section if you want
                    }
                }));


        // create sections
        MaterialSection section = newSection("Section 1", new FractionIndex());
        addSection(section);
        section.setSelected(true);
        currentSection = section;

        addSection(newSection("Section 2", new FractionIndex()));
        addSection(newSectionWithRealColor("Section 3",
                ResourceUtil.getPixelMapElement(this, ResourceTable.Media_ic_mic_white_24dp),
                new FractionButton()).setSectionColor(Color.getIntColor("#9c27b0")));
        addSection(newSection("Section",
                ResourceUtil.getPixelMapElement(this, ResourceTable.Media_ic_hotel_grey600_24dp),
                new FractionButton()).setSectionColor(Color.getIntColor("#03a9f4")));

        // create bottom section
        this.addBottomSection(newSection("Bottom Section",
                ResourceUtil.getPixelMapElement(this, ResourceTable.Media_ic_settings_black_24dp),
                createIntent("it.neokree.example.Settings")));
    }
}
