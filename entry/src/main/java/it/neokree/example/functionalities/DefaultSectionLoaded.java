package it.neokree.example.functionalities;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

public class DefaultSectionLoaded extends FractionAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(DefaultSectionLoadedSlice.class.getName());
    }
}
