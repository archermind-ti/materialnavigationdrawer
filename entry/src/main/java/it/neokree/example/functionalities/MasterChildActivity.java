package it.neokree.example.functionalities;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

public class MasterChildActivity extends FractionAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MasterChildActivitySlice.class.getName());
    }
}
