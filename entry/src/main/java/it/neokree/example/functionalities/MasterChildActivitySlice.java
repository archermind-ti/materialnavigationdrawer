package it.neokree.example.functionalities;

import it.neokree.example.mockedfraction.FractionMaster;
import it.neokree.materialnavigationdrawer.ResourceTable;
import it.neokree.materialnavigationdrawer.elements.MaterialSection;
import it.neokree.materialnavigationdrawer.slice.DrawerAbilitySlice;
import it.neokree.materialnavigationdrawer.util.ResourceUtil;
import ohos.aafwk.content.Intent;

public class MasterChildActivitySlice extends DrawerAbilitySlice {
    @Override
    public void init(Intent intent) {
        this.setDrawerHeaderImage(ResourceUtil.getPixelMapElement(this, ResourceTable.Media_mat3));

        MaterialSection section = this.newSection("Section 1", new FractionMaster());
        this.addSection(section);
        section.setSelected(true);
        currentSection = section;

        this.addSection(this.newSection("Section 2", new FractionMaster()));
    }
}
