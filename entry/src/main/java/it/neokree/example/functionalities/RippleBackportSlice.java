package it.neokree.example.functionalities;

import it.neokree.example.mockedfraction.FractionButton;
import it.neokree.example.mockedfraction.FractionIndex;
import it.neokree.materialnavigationdrawer.ResourceTable;
import it.neokree.materialnavigationdrawer.elements.MaterialSection;
import it.neokree.materialnavigationdrawer.slice.DrawerAbilitySlice;
import it.neokree.materialnavigationdrawer.util.ResourceUtil;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;

public class RippleBackportSlice extends DrawerAbilitySlice {
    @Override
    public void init(Intent intent) {
        // create sections
        MaterialSection section = newSection("Section 1", new FractionIndex());
        this.addSection(section);
        section.setSelected(true);
        currentSection = section;

        this.addSection(newSection("Section 2", new FractionIndex()));
        this.addSection(newSectionWithRealColor("Section 3",
                ResourceUtil.getPixelMapElement(this, ResourceTable.Media_ic_mic_white_24dp),
                new FractionButton()).setSectionColor(Color.getIntColor("#9c27b0")));
        this.addSection(newSection("Section 4",
                ResourceUtil.getPixelMapElement(this, ResourceTable.Media_ic_hotel_grey600_24dp),
                new FractionButton()).setSectionColor(Color.getIntColor("#03a9f4")));

        // create bottom section
        this.addBottomSection(newSection("Bottom Section",
                ResourceUtil.getPixelMapElement(this, ResourceTable.Media_ic_settings_black_24dp),
                createIntent("it.neokree.example.Settings")));
    }
}
