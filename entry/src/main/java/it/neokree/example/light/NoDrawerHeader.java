package it.neokree.example.light;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

public class NoDrawerHeader extends FractionAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(NoDrawerHeaderSlice.class.getName());
    }
}
