package it.neokree.example.mockedfraction;

import it.neokree.example.ResourceTable;
import it.neokree.materialnavigationdrawer.ThemeColor;
import it.neokree.materialnavigationdrawer.util.ColorUtil;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

/**
 * create by DaiL on 2021/5/27
 */
public class FractionButton extends Fraction {
    private boolean isLightMode = true;

    public FractionButton() {
    }

    public FractionButton(boolean isLightMode) {
        this.isLightMode = isLightMode;
    }
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_fraction_button, container, false);
        Text text = (Text) component.findComponentById(ResourceTable.Id_text);
        if (!isLightMode){
            text.setTextColor(ColorUtil.getColor(ThemeColor.COLOR_MODE_DARK_TEXT));
        }
        return component;
    }
}
