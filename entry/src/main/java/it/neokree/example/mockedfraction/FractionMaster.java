package it.neokree.example.mockedfraction;

import it.neokree.example.ChildAbility;
import it.neokree.example.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

/**
 * create by DaiL on 2021/5/28
 */
public class FractionMaster extends Fraction {
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_fraction_index, container, false);
        initView(component);
        return component;
    }

    private void initView(Component component) {
        Text text = (Text) component.findComponentById(ResourceTable.Id_text);
        text.setText("OPEN CHILD");

        text.setClickedListener(component1 -> {
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getFractionAbility().getBundleName())
                    .withAbilityName(ChildAbility.class)
                    .build();
            intent.setOperation(operation);
            FractionAbility ability = getFractionAbility();
            ability.startAbility(intent);
        });
    }
}
