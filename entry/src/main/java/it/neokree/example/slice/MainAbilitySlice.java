package it.neokree.example.slice;

import it.neokree.example.ResourceTable;
import it.neokree.example.light.MockedAccountSlice;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

public class MainAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initView();
    }

    private void initView() {
        findComponentById(ResourceTable.Id_text_Light_Mocked_Account).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_Dark_Mocked_Account).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_Light_Accounts).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_Dark_Accounts).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_Light_Drawer_Header_Image).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_Dark_Drawer_Header_Image).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_Light_No_Drawer_Header).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_Dark_No_Drawer_Header).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_Light_Custom_Drawer_Header).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_Dark_Custom_Drawer_Header).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_Functionality_unique_Toolbar_Color).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_ripple_backport_support).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_multi_pane_support_for_tablet).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_custom_section_under_account_list).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_Kitkat_trasluncent_status_bar).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_Master_Child_example).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_section_not_pre_rendered).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_default_section_loaded).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_action_bar_shadow_enabled).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_learning_pattern_disabled).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_Back_To_first).setClickedListener(clickedListener);
        findComponentById(ResourceTable.Id_text_Back_Anywhere).setClickedListener(clickedListener);
    }

    private Component.ClickedListener clickedListener = component -> {
        int componentId = component.getId();
        String abilityName = "";
        if (componentId == ResourceTable.Id_text_Light_Mocked_Account) {
            abilityName = "it.neokree.example.light.MockedAccount";
        } else if (componentId == ResourceTable.Id_text_Dark_Mocked_Account) {
            abilityName = "it.neokree.example.dark.MockedAccount";
        } else if (componentId == ResourceTable.Id_text_Light_Accounts) {
            abilityName = "it.neokree.example.light.Accounts";
        } else if (componentId == ResourceTable.Id_text_Dark_Accounts) {
            abilityName = "it.neokree.example.dark.Accounts";
        } else if (componentId == ResourceTable.Id_text_Light_Drawer_Header_Image) {
            abilityName = "it.neokree.example.light.ImageDrawerHeader";
        } else if (componentId == ResourceTable.Id_text_Dark_Drawer_Header_Image) {
            abilityName = "it.neokree.example.dark.ImageDrawerHeader";
        } else if (componentId == ResourceTable.Id_text_Light_No_Drawer_Header) {
            abilityName = "it.neokree.example.light.NoDrawerHeader";
        } else if (componentId == ResourceTable.Id_text_Dark_No_Drawer_Header) {
            abilityName = "it.neokree.example.dark.NoDrawerHeader";
        } else if (componentId == ResourceTable.Id_text_Light_Custom_Drawer_Header) {
            abilityName = "it.neokree.example.light.CustomDrawerHeader";
        } else if (componentId == ResourceTable.Id_text_Dark_Custom_Drawer_Header) {
            abilityName = "it.neokree.example.dark.CustomDrawerHeader";
        } else if (componentId == ResourceTable.Id_text_Functionality_unique_Toolbar_Color) {
            abilityName = "it.neokree.example.functionalities.UniqueToolbarColor";
        } else if (componentId == ResourceTable.Id_text_ripple_backport_support) {
            abilityName = "it.neokree.example.functionalities.RippleBackport";
        } else if (componentId == ResourceTable.Id_text_multi_pane_support_for_tablet) {
            abilityName = "it.neokree.example.functionalities.MultiPane";
        } else if (componentId == ResourceTable.Id_text_custom_section_under_account_list) {
            abilityName = "it.neokree.example.functionalities.CustomAccountSection";
        } else if (componentId == ResourceTable.Id_text_Kitkat_trasluncent_status_bar) {
            abilityName = "it.neokree.example.functionalities.KitkatStatusBar";
        } else if (componentId == ResourceTable.Id_text_Master_Child_example) {
            abilityName = "it.neokree.example.functionalities.MasterChildActivity";
        } else if (componentId == ResourceTable.Id_text_section_not_pre_rendered) {
            abilityName = "it.neokree.example.functionalities.RealColorSections";
        } else if (componentId == ResourceTable.Id_text_default_section_loaded) {
            abilityName = "it.neokree.example.functionalities.DefaultSectionLoaded";
        } else if (componentId == ResourceTable.Id_text_action_bar_shadow_enabled) {
            abilityName = "it.neokree.example.functionalities.ActionBarShadow";
        } else if (componentId == ResourceTable.Id_text_learning_pattern_disabled) {
            abilityName = "it.neokree.example.functionalities.LearningPatternDisabled";
        } else if (componentId == ResourceTable.Id_text_Back_To_first) {
            abilityName = "it.neokree.example.backpattern.BackToFirst";
        } else if (componentId == ResourceTable.Id_text_Back_Anywhere) {
            abilityName = "it.neokree.example.backpattern.BackAnywhere";
        }

        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(getBundleName())
                .withAbilityName(abilityName)
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    };

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
